var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var userModel = require('../../models/users.js');
var User=mongoose.model('User');

router.use(function(req, res, next){        
    if(req.method === "GET" || req.method === "POST"){              
        if(!req.isAuthenticated()) {            
            res.send({redirect:'/#login'});
        }else{
            return next();
        }
    }
});

router.route('/getCurrentUser').post(function(req, res){    
    User.findById(req.session.passport.user, function(err, user){
       if(err){
           console.log(err);
           return res.send(500, err);
       }else{           
           return res.json(user);
       }
    });
});

router.route('/getAllusers').get(function(req, res){
   User.find({}, function(err, users){
      if(err) {
          console.log(err);
          return res.send(500, err);
      }else{
          return res.json(users);
      }
   }); 
});

module.exports = router;