var express = require('express');
var router = express.Router();

module.exports = function(passport){
  router.get('/success', function(req,res){      
      res.send({message: 'success', userID:req.session.passport.user});
  });  
  
  router.get('/failure', function(req,res){            
    res.send({state: 'failure', user: null, message: req.session.error});
    req.session.destroy();
  });
  
  router.post('/login', passport.authenticate('login', {
      successRedirect: 'success',
      failureRedirect: 'failure'
  }));        
    
  router.get('/signout', function(req, res){
     req.logout();
     req.session.destroy();
     res.redirect('/#login');
  });       
  return router;    
};