var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');

module.exports =function(passport){
    
    passport.serializeUser(function(user, done){       
        console.log('serializing user: '+user._id);
        return done(null, user._id);
    });

    passport.deserializeUser(function(id, done){
        User.findById(id,function(err, user){
            if(err){
                return done(err, false);
            }

            if(!user){
                console.log('user not found');
                return done('user not found', false);
            }
            return done(null, user);
        });
    });

    passport.use('login', new LocalStrategy({
            passReqToCallback : true
        },
        function(req, username, password, done) {            
            User.findOne({username: req.body.username}, function(err, user){
                if(err){
                    console.log(err);
                    return done(err, false);
                }
                              
                //if there is no user with this username
                if(!user){
                    req.session.error = '001';
                    console.log('username not found');
                    return done(null, false, {message: 'user' +username+ ' not found!'});
                }

                //if wrong password is introduced                
               if(req.body.password!=user.password){
                    req.session.error = '002';
                    console.log('wrong password');
                    return done(null, false, {message: 'Wrong password'});
                }

               req.session.regenerate(function(){
                 req.session.user = user;
                 req.session.success = 'Atuthenticated as '+ user.name + " click to <a href=\"public/login.html\">logout</a>.  You may now access <a href=\"/\">/restricted</a>.";
               });               
               return done(null,user);
           });
        }
    ));
};