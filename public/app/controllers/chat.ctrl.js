(function(){
  angular.module('elephantChat').controller( 'ChatController', function($scope, $rootScope, $http, $location) {      
    $http({
           method:'POST',
           url: '/accessToChat/getCurrentUser',
           data: ''
        }).then(function successCallback(response){                        
            if(response.data.redirect=="/#login"){
                $location.path('/login');
                $rootScope.isAuthenticated=false;
                return;
            }
            $rootScope.isAuthenticated=true;
            $scope.user=response.data;
            localStorage.setItem('chat-username',$scope.user.username);
        });  
    $scope.messages = [];    
     try{
        $rootScope.IoSocket.on('reply',function(data){
           var notificationSound = new Audio('../../assets/sound/sms-alert-1-daniel_simon.mp3');
           notificationSound.play(); 
           var info=data;                               
           $scope.messages.push({username:info.data.from, date:new Date(), message:info.data.message, type:'incomingMessage'}); 
        });
     }catch(err){         
         $rootScope.IoSocket = io.connect('192.168.1.102:1337', {query: "foo=" + localStorage.getItem('userId')});  
     }
      
    $scope.selectContact=function(contact){
        if(contact.username!=$scope.user.username){
          $rootScope.selectedContact=contact;   
        }        
    };
      
    $scope.send=function(){                       
        if($rootScope.selectedContact){
          $rootScope.IoSocket.emit('message', {to:$rootScope.selectedContact._id,message:$scope.message,from:$scope.user.username});   
          $scope.messages.push({username:$scope.user.username, date:new Date(), message:$scope.message, type:'answer'});    
        }else{
         $rootScope.IoSocket.emit('broadcastMessage', {message:$scope.message, from:$scope.user.username});   
          $scope.messages.push({username:$scope.user.username, date:new Date()+" Broadcast Message!!!", message:$scope.message, type:'answer'});  
        }                   
       $scope.message="";
    };
} );
}());
