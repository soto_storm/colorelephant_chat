(function(){
  angular.module('elephantChat').controller( 'ContactsController', function($scope, $rootScope, $http) {   
      $scope.allContacts=[];
      $http({
           method:'GET',
           url: '/accessToChat/getAllUsers',
           data: ''
        }).then(function successCallback(response){                   
            $scope.allContacts=response.data;
            for(var i=0; i<$scope.allContacts.length; i++){
                $scope.allContacts[i].status="disconnected";
            }
        });
      
    try{
        $rootScope.IoSocket.on('newUserConnected',function(data){
            for(var i=0; i<$scope.allContacts.length; i++){                
                if(data.contact==$scope.allContacts[i]._id){
                    $scope.allContacts[i].status="connected";
                }
            }
        });
        $rootScope.IoSocket.on('usersConnected', function(data){
            console.log(data);
        });
     }catch(err){
         $rootScope.IoSocket = io.connect('192.168.1.102:1337', {query: "foo=" + localStorage.getItem('userId')});  
     }
      
  });
}());