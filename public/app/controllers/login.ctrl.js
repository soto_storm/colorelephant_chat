(function(){
  angular.module('elephantChat').controller('loginCtrl',
    function($rootScope, $scope, $http, $location){        
    if($rootScope.isAuthenticated){
        $location.path('/'); 
        return;
    }
        $scope.user={};
        $scope.error={};
        $scope.user.username = localStorage.getItem('chat-username');
        $scope.login=function(form){
            if(form.$valid){
                $http({
                   method:'POST',
                   url: '/auth/login',
                   data: $scope.user
                }).then(function successCallback(response){                    
                    switch(response.data.message){
                        case '001': $scope.error={
                            status:true,
                            message:'Username does not exist.'
                        }
                        break;
                        case '002':$scope.error={
                            status:true,
                            message:'Wrong password.'
                        }
                        break;
                        case 'success':$scope.error={
                            status:false,
                            message:''
                        }
                        $location.path('/'); 
                        $rootScope.isAuthenticated=true;
                        localStorage.setItem('userId', response.data.userID);
                        break;
                    }
                });
            }
        };
       });
}());
