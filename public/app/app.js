/**
 * Created by Jesus Soto on 4/26/2017.
 */

var app=angular.module('elephantChat', ['ngRoute','btford.socket-io', 'ngMaterial']).run(function($rootScope){ 
    //if($rootScope.isAuthenticated){
        $rootScope.IoSocket = io.connect('192.168.1.102:1337', {query: "foo=" + localStorage.getItem('userId')});  
    //}
});



app.config(function($routeProvider){
    $routeProvider.when('/', {
        templateUrl: 'app/views/home.html',
        controller: ''
    })

    .when('/login',{
        templateUrl: 'app/views/login.html'
    })

    .when('/signup',{
        templateUrl: 'app/views/signup.html',
        controller:''
    })
    
    .otherwise({redirectTo:'/'});
});