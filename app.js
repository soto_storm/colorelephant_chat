var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var session = require('express-session');
var mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);

//db connection
mongoose.connect("mongodb://127.0.0.1:27017/color_e_chat");

//routes calling
var index = require('./routes/index');
var authenticate  = require('./routes/authentication/authenticate')(passport);
var accessToChat = require('./routes/chat/access');

var app = express();
var server= app.listen(1337)
var io = require('socket.io').listen(server);
var clients = [];

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: 'aiotik',
    clear_interval: 900,
    cookie:{maxAge: 720*60*60*1000},
    store: new MongoStore({
        mongooseConnection: mongoose.connection
    })
}));

app.use(passport.initialize());
app.use(passport.session());
app.use('/', index);
app.use('/auth', authenticate);
app.use('/accessToChat', accessToChat);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
//connection socket.io
io.on('connection', function(socket){
  if(socket.handshake.query.foo==='null'){}else{
        console.warn('new connection for '+socket.id+' userID:'+socket.handshake.query.foo);
        clients.push({id:socket.id, user:socket.handshake.query.foo});
        socket.emit('newUserConnected', {
            contact: socket.handshake.query.foo,
            status: 'connected'
        });
        socket.to(socket.id).emit('usersConnected',{
            users: clients 
        });
    }
  
   socket.on('disconnect', function(){
        for(var i=0; i<clients.length; i++){
            if(clients[i].id===socket.id){                
                socket.emit('userDisconnected', {
                   user:  clients[i].user
                });
                clients.splice(i,1);                                
            }
        }
       console.warn('user disconnected '+socket.id);      
   });    
      
  socket.on('message', function(data){    
    for(var i=0; i<clients.length; i++)  {
        if(clients[i].user==data.to){
             socket.to(clients[i].id).emit('reply', {
                data:{from:data.from, message:data.message}
             });
        }
     }
  });
    
  socket.on('broadcastMessage', function(data){     
     for(var i=0; i<clients.length; i++)  {
         socket.to(clients[i].id).emit('reply', {
            data:data
        });
     }
  });
    
});
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

require('./models/users.js');
var User = mongoose.model('User');
var initPassport = require('./routes/authentication/passport-init');
initPassport(passport);

module.exports = app;
