# README #

To execute this chat you have to make sure you got the latest version of Node.js.

These are the steps to execute the chat:

- Install node dependencies: run command "npm install"
- Install bower dependencies, go to public directory: run command "bower install"
- Generate test users. Go to resources directory and execute this command: " mongoimport --db color_e_chat --collection users --type json --file users.json --jsonArray"
- In the files contacts.ctrl.js and chat.ctrl.js put your PC IP in order for the socket.io to stablish connection. (Not localhost or 127.0.0.1, your PC IP).

Contact me through my website: sotous.com, if you have any issues.